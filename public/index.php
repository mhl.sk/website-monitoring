<?php 

    require __DIR__ . "/../vendor/autoload.php"; 

    use Medoo\Medoo;
    use Symfony\Component\Mailer\Transport;
    use Symfony\Component\Mailer\Mailer;
    use Symfony\Component\Mime\Email;
    use Symfony\Component\Mime\Address;

    class Monitoring
    {

        private $guzzle;
        private $database;
        private $mailer;
        private $domains;

        public function __construct()
        {
            $this->loadENV();
            $this->debugging();
            $this->connectToDB();
            $this->initiateGuzzle();
            $this->initiateMailer();
            $this->getDomains();
            $this->checkDomains();
        }

        private function loadENV() : void
        {
            Dotenv\Dotenv::createImmutable(__DIR__."/../")->load();
        }

        private function debugging() : void
        {
            if(filter_var($_ENV['APP_DEBUG'], FILTER_VALIDATE_BOOLEAN) == true) {
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);
            }
        }

        private function connectToDB() : void
        {
            $this->database = new Medoo([
                'type' => $_ENV['DB_TYPE'],
                'host' => $_ENV['DB_HOST'],
                'database' => $_ENV['DB_NAME'],
                'username' => $_ENV['DB_USERNAME'],
                'password' => $_ENV['DB_PASSWORD']
            ]);
        }

        private function initiateGuzzle() : void
        {
            $this->guzzle = new \GuzzleHttp\Client();
        }

        private function initiateMailer() : void
        {
            $this->mailer = new Mailer(Transport::fromDsn($_ENV['GMAIL_DSN']));
        }

        private function getDomains() : void
        {
            $this->domains = $this->database->select("domains", "*");
        }

        private function checkDomains() : void
        {
            foreach($this->domains as $key => $domain) {
                $this->getStatusCode($key);
                $this->updateDatabase($key);
            }
        }

        private function getStatusCode($key) : void
        {
            if($this->domains[$key]['domain'] == gethostbyname($this->domains[$key]['domain'])) {
                $this->domains[$key]['current_status'] = 404;
            } else {
                $this->domains[$key]['current_status'] = $this->guzzle->request('GET', $this->domains[$key]['domain'])->getStatusCode();
            }
        }

        private function updateDatabase($key) : void
        {
            if($this->domains[$key]['current_status'] != $this->domains[$key]['status']) {
                $this->database->update(
                    "domains", 
                    [
                        "status" => $this->domains[$key]['current_status']
                    ], 
                    [
                        "id"=> $this->domains[$key]['id']
                    ]
                );
                $this->updateTimestamp($key);
                $this->prepareEmail($key);
            }
        }

        private function updateTimestamp($key) : void
        {
            $this->database->update(
                "domains", 
                [
                    "timestamp" => $this->domains[$key]['current_status'] == 200 ? null : time()
                ], 
                [
                    "id"=> $this->domains[$key]['id']
                ]
            );
        }

        private function prepareEmail($key) : void
        {
            if($this->domains[$key]['current_status'] == 200) {
                $down_for = gmdate("H:i:s", time() - $this->domains[$key]['timestamp']);
                $this->sendEmail(
                    "Website up - " . $this->domains[$key]['domain'],
                    "Website <b>" . $this->domains[$key]['domain'] . "</b> is up again.<br><br>It was down for <b>" . $down_for . "</b><br><br>Kind regards,<br>" . $_ENV['NOTIFICATION_SENDER_NAME']
                );
            } else {
                $this->sendEmail(
                    "Website down - " . $this->domains[$key]['domain'],
                    "Website <b>" . $this->domains[$key]['domain'] . "</b> is down. <br><br><b>Status code:</b> " . $this->domains[$key]['current_status'] . "<br><br>Kind regards,<br>" . $_ENV['NOTIFICATION_SENDER_NAME']
                );
            }
        }

        private function sendEmail($subject, $message) : void
        {
            $email = new Email();
            $email->from(new Address($_ENV['GMAIL_USERNAME'], $_ENV['NOTIFICATION_SENDER_NAME']));
            $email->to($_ENV['NOTIFICATION_RECEIVER_EMAIL']);
            $email->subject($subject);
            $email->html($message); 
            try { 
                $this->mailer->send($email); 
            } catch (TransportExceptionInterface $e) { 
                throw new Exception($e->getMessage()); 
            }
        }

    } 
    
    if (php_sapi_name() == "cli") {
        new Monitoring();
    } else {
        die("Script execution is only allowed via CRON");
    }
    
?>