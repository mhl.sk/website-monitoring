## Installation guide

1. Composer install 
2. Rename .env.example to .env
3. Create local database, import sql file from /database
4. Add domains you want to track to this database
5. Update .env file
6. Deploy app to your hosting
7. Setup CRONJOB with desired interval